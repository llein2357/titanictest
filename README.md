# TitanicTest

run_model.sh - запускает подготовку Feature Engineering, fit/save
run_spark.sh - выполнет задачи, связанные с таблицами Spark
run_api.sh - запускате RestApi (по-умолчанию http://localhost:5000/)

Модель: используется sklearn RandomForestClassifier (111 деревьев, лимит в 3 уровня глубины)
Features: dummy-признаки, сформированные из базовых "Pclass", "Sex", "SibSp", "Parch"


# REST-API
Реализовано на Flask.
Test-case для проверки запрашиваего функционала API:
curl -i -H "Content-Type: application/json" -X POST -d '{"Pclass":1, "Sex":"male", "SibSp":0, "Parch":0}' http://localhost:5000/todo/api/v1.0/tasks

Дополнительный функционал API :
/todo/api/v1.0/tasks/<int:task_id> - просмотр содержимого по id
/todo/api/v1.0/tasks (с параметром GET) - просмотр списка всех пассажиров (включая добавленных в API)


# Const
В файле const.py можно поменять расположение на более удобно/необходимое для тестирования

